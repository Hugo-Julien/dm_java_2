import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.size() == 0)
          return null;
        Integer res = liste.get(0);
        for (Integer elem : liste){
          if (elem < res)
            res = elem;
        }
        return res;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeur donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
      if (liste.size() == 0)
        return true;
      for (T elem : liste){
        if (elem.compareTo(valeur) <= 0)
          return false;
      }
      return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> res = new ArrayList<>();
      if (liste1.size() == 0 || liste2.size() == 0)
        return res;
      int i = 0;
      int j = 0;
      while (i < liste1.size() && j < liste2.size()){
        T elem1 = liste1.get(i);
        T elem2 = liste2.get(j);
        if (elem1.equals(elem2) && !(res.contains(elem1)))
          res.add(elem1);
        if (elem1.compareTo(elem2) < 0){
          i += 1;
        }
        else{
          j += 1;
        }
      }
      return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      List<String> res = new ArrayList<>();
      if (texte.equals(""))
        return res;
      String[] mots = texte.split(" ");
      for (String mot : mots)
        if (!mot.isEmpty())
          res.add(mot);
      return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      if (texte.isEmpty())
        return null;
      Map<String,Integer> dico = new HashMap<>();
      List<String> lMots = decoupe(texte);
      for (String mot : lMots){
        if (dico.containsKey(mot)){
          dico.put(mot, dico.get(mot)+1);
        }
        else{
          dico.put(mot, 1);
        }
      }
      Collections.sort(lMots);
      String res = lMots.get(0);
      for (String mot : lMots){
        if (dico.get(res) < dico.get(mot))
          res = mot;
      }
      return res;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int ouv = 0;
      int fer = 0;
      for (int i=0 ; i<chaine.length() ; i++){
        if (chaine.charAt(i) == '(')
          ouv += 1;
        else if (chaine.charAt(i) == ')')
          fer += 1;
        if (fer > ouv)
          return false;
      }
      if (fer != ouv)
        return false;
      return true;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      int pOuv = 0;
      int pFer = 0;
      int cOuv = 0;
      int cFer = 0;
      for (int i=0 ; i<chaine.length() ; i++){
        if (chaine.charAt(i) == '('){
          pOuv += 1;
          if (i+1<chaine.length() && chaine.charAt(i+1) == ']')
            return false;
        }
        else if (chaine.charAt(i) == ')'){
          pFer += 1;
          if (i>0 && chaine.charAt(i-1) == '[')
            return false;
        }
        else if (chaine.charAt(i) == '['){
          cOuv += 1;
          if (i+1<chaine.length() && chaine.charAt(i+1) == ')')
            return false;
        }
        else if (chaine.charAt(i) == ']'){
          cFer += 1;
          if (i>0 && chaine.charAt(i-1) == '(')
            return false;
        }

        if (pFer > pOuv || cFer > cOuv)
          return false;
      }
      if (pFer != pOuv || cFer != cOuv)
        return false;
      return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.isEmpty())
          return false;
        int deb = 0;
        int fin = liste.size()-1;
        int mil;
        while (fin - deb > 1){
          mil = (deb+fin)/2;
          if (liste.get(mil) > valeur){
            fin = mil;
          }
          else if (liste.get(mil) < valeur){
            deb = mil;
          }
          if (liste.get(mil).equals(valeur) || liste.get(deb).equals(valeur) || liste.get(fin).equals(valeur))
            return true;
        }
        return false;
    }



}
